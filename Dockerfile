# BUILD
FROM maven:latest as build_backend
ENV LANG C.UTF-8
WORKDIR /app
COPY pom.xml /app/pom.xml
COPY src /app/src
COPY mvnw /app/mvnw
COPY mvnw.cmd /app/mvnw.cmd
COPY .mvn /app/.mvn
RUN mvn clean package

# APP
FROM registry.gitlab.com/hellsoft-colombia/cpplugins-3.0-server/cpplugins-base:lastest

RUN apk add openjdk11
ENV JAVA_HOME="/usr/lib/jvm/java-11-openjdk/"
ENV PATH=$PATH:${JAVA_HOME}/bin

#Add Project Folder
ENV APP_PORT=5000
ARG LIBRARY_WRAPPER=./src/main/
ADD ${LIBRARY_WRAPPER}/native /tmp/cpplugins/native
COPY --from=build_backend /app/target/cpplugins-0.0.1-SNAPSHOT.jar /tmp/cpplugins/app.jar
ADD ./CMakeLists.txt /tmp/cpplugins/native/CMakeLists.txt
RUN mkdir -p /tmp/cpplugins/storage && mkdir -p /opt/lib && mkdir -p /tmp/cpplugins/lib && cd /tmp/cpplugins/native && mkdir /tmp/cpplugins/native/build && cd /tmp/cpplugins/native/build && cmake .. && make && ln -sf /tmp/cpplugins/native/build/libcpPluginsJNI.so /usr/local/lib/libcpPluginsJNI.so

EXPOSE 5000 8161 61616
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=container","-Xmx3000m","-jar","/tmp/cpplugins/app.jar"]
