package co.edu.javeriana.hellsoft.cpplugins.controller.api;

import co.edu.javeriana.hellsoft.cpplugins.controller.request.NewCustomPluginRequest;
import co.edu.javeriana.hellsoft.cpplugins.controller.request.UpdateCustomPluginRequest;
import co.edu.javeriana.hellsoft.cpplugins.dto.model.CustomPluginDto;
import co.edu.javeriana.hellsoft.cpplugins.dto.response.Response;
import co.edu.javeriana.hellsoft.cpplugins.model.User;
import co.edu.javeriana.hellsoft.cpplugins.model.pipeline.PipelineDescriptor;
import co.edu.javeriana.hellsoft.cpplugins.service.core.CustomPluginService;
import co.edu.javeriana.hellsoft.cpplugins.service.core.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/cpPlugins/custom-plugin")
class CustomPluginController {
    @Autowired
    private CustomPluginService customPluginService;

    @Autowired
    private UserService userService;

    @GetMapping("")
    public Response getUserCustomPlugins() {
        User user = getAuthenticatedUser();
        List<CustomPluginDto> userPipelines = customPluginService.getUserCustomPlugins(user);
        return Response.ok().setPayload(userPipelines);
    }

    @GetMapping("/{pluginId}")
    public Response getCustomPluginById(@PathVariable String pluginId) {
        User user = getAuthenticatedUser();
        PipelineDescriptor descriptor = customPluginService.findCustomPluginById(user, pluginId);
        return Response.ok().setPayload(descriptor);
    }

    @PostMapping("/new")
    public Response newCustomPlugin(
            @RequestBody @Valid NewCustomPluginRequest newCustomPluginRequest) {
        User user = getAuthenticatedUser();
        CustomPluginDto newCustomPlugin =
                customPluginService.addCustomPlugin(
                        user,
                        newCustomPluginRequest.getPluginName(),
                        newCustomPluginRequest.getDescriptor(),
                        newCustomPluginRequest.getInputs(),
                        newCustomPluginRequest.getOutputs(),
                        newCustomPluginRequest.getSubtitle());
        return Response.ok().setPayload(newCustomPlugin);
    }

    @PutMapping("/update")
    public Response updatePipeline(
            @RequestBody @Valid UpdateCustomPluginRequest updateCustomPluginRequest) {
        User user = getAuthenticatedUser();
        CustomPluginDto newCustomPlugin =
                customPluginService.updateCustomPlugin(
                        user,
                        updateCustomPluginRequest.getPluginId(),
                        updateCustomPluginRequest.getPluginName(),
                        updateCustomPluginRequest.getDescriptor(),
                        updateCustomPluginRequest.getInputs(),
                        updateCustomPluginRequest.getOutputs(),
                        updateCustomPluginRequest.getSubtitle());
        return Response.ok().setPayload(newCustomPlugin);
    }

    @DeleteMapping("/delete/{pluginId}")
    public Response removePipeline(@PathVariable String pluginId) {
        User user = getAuthenticatedUser();
        customPluginService.removeCustomPlugin(user, pluginId);
        return Response.ok().setPayload("Plugin removed");
    }

    private User getAuthenticatedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return userService.findNativeUserByUserName((String) auth.getPrincipal());
    }
}
