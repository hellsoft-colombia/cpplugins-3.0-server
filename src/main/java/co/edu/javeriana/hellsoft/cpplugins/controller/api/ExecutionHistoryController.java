package co.edu.javeriana.hellsoft.cpplugins.controller.api;

import co.edu.javeriana.hellsoft.cpplugins.controller.request.NewExecutionHistoryEntryRequest;
import co.edu.javeriana.hellsoft.cpplugins.dto.model.ExecutionHistoryDto;
import co.edu.javeriana.hellsoft.cpplugins.dto.response.Response;
import co.edu.javeriana.hellsoft.cpplugins.model.User;
import co.edu.javeriana.hellsoft.cpplugins.service.core.ExecutionHistoryService;
import co.edu.javeriana.hellsoft.cpplugins.service.core.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/cpPlugins/history")
class ExecutionHistoryController {
    @Autowired
    private ExecutionHistoryService executionHistoryService;
    @Autowired
    private UserService userService;

    @GetMapping("/{page}/{elements}")
    public Response getUserHistory(@PathVariable int page, @PathVariable int elements) {
        User user = getAuthenticatedUser();
        List<ExecutionHistoryDto> executionHistoryDtoList =
                executionHistoryService.getUserExecutionHistory(user, elements, page);
        return Response.ok().setPayload(executionHistoryDtoList);
    }

    @GetMapping("/count")
    public Response getUserHistoryCount() {
        User user = getAuthenticatedUser();
        return Response.ok().setPayload(executionHistoryService.getUserExecutionHistoryCount(user));
    }

    @GetMapping("/count/{state}")
    public Response getUserHistoryByStateCount(@PathVariable String state) {
        User user = getAuthenticatedUser();
        return Response.ok()
                .setPayload(executionHistoryService.getUserExecutionHistoryByStateCount(user, state));
    }

    @GetMapping("/{state}/{page}/{elements}")
    public Response getUserHistoryByState(
            @PathVariable String state, @PathVariable int page, @PathVariable int elements) {
        User user = getAuthenticatedUser();
        List<ExecutionHistoryDto> executionHistoryDtoList =
                executionHistoryService.getUserExecutionHistoryByState(user, state, elements, page);
        return Response.ok().setPayload(executionHistoryDtoList);
    }

    @GetMapping("/{entryId}")
    public Response getUserHistoryByState(@PathVariable String entryId) {
        User user = getAuthenticatedUser();
        ExecutionHistoryDto executionHistoryDto =
                executionHistoryService.getExecutionHistoryById(user, entryId);
        return Response.ok().setPayload(executionHistoryDto);
    }

    @PostMapping("/new")
    public Response newEntry(@RequestBody @Valid NewExecutionHistoryEntryRequest request) {
        User user = getAuthenticatedUser();
        executionHistoryService.addHistoryReference(
                request.getExecutionState(),
                request.getPipelineIdReference(),
                request.getPipelineNameReference(),
                request.getPipelineDescriptionReference(),
                request.getExecutionResult(),
                user,
                new ArrayList<>());
        return Response.ok().setPayload("Added");
    }

    private User getAuthenticatedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return userService.findNativeUserByUserName((String) auth.getPrincipal());
    }
}
