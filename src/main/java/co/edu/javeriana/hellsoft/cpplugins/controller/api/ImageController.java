package co.edu.javeriana.hellsoft.cpplugins.controller.api;

import co.edu.javeriana.hellsoft.cpplugins.dto.model.ImageDto;
import co.edu.javeriana.hellsoft.cpplugins.dto.response.Response;
import co.edu.javeriana.hellsoft.cpplugins.exception.CpPluginException;
import co.edu.javeriana.hellsoft.cpplugins.model.User;
import co.edu.javeriana.hellsoft.cpplugins.service.core.ImageService;
import co.edu.javeriana.hellsoft.cpplugins.service.core.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

import static co.edu.javeriana.hellsoft.cpplugins.exception.EntityType.IMAGE;
import static co.edu.javeriana.hellsoft.cpplugins.exception.ExceptionType.BAD_FORMATTED_DATA;

@RestController
@RequestMapping("/cpPlugins/image")
class ImageController {

    @Autowired
    private ImageService imageService;

    @Autowired
    private UserService userService;

    @GetMapping("")
    public Response getUserImages() {
        User user = getAuthenticatedUser();
        List<ImageDto> images = imageService.findUserImages(user);
        return Response.ok().setPayload(images);
    }

    @GetMapping("/{imageId}")
    public ResponseEntity<Resource> getImage(@PathVariable String imageId) {
        User user = getAuthenticatedUser();
        Resource image = imageService.getImage(user, imageId);
        return ResponseEntity.ok()
                .header(
                        HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + image.getFilename() + "\"")
                .body(image);
    }

    @PostMapping("/new")
    public Response newImage(
            @RequestParam MultipartFile file,
            @RequestParam String fileName,
            @RequestParam String format,
            @RequestParam String size) {
        User user = getAuthenticatedUser();
        ImageDto imageDto;
        try {
            imageDto =
                    imageService.addImage(user, fileName, format, Float.parseFloat(size), file.getBytes());
        } catch (IOException e) {
            throw CpPluginException.throwException(IMAGE, BAD_FORMATTED_DATA, file.getOriginalFilename());
        }
        return Response.ok().setPayload(imageDto);
    }

    @DeleteMapping("/delete/{imageId}")
    public Response removePipeline(@PathVariable String imageId) {
        User user = getAuthenticatedUser();
        imageService.removeImage(user, imageId);
        return Response.ok().setPayload("Image removed");
    }

    private User getAuthenticatedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return userService.findNativeUserByUserName((String) auth.getPrincipal());
    }
}
