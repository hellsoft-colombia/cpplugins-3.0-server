package co.edu.javeriana.hellsoft.cpplugins.controller.api;

import co.edu.javeriana.hellsoft.cpplugins.controller.request.ExecutePipelineRequest;
import co.edu.javeriana.hellsoft.cpplugins.controller.request.NewPipelineRequest;
import co.edu.javeriana.hellsoft.cpplugins.controller.request.UpdatePipelineRequest;
import co.edu.javeriana.hellsoft.cpplugins.dto.model.PipelineDto;
import co.edu.javeriana.hellsoft.cpplugins.dto.response.Response;
import co.edu.javeriana.hellsoft.cpplugins.messages.FinishedPipeline;
import co.edu.javeriana.hellsoft.cpplugins.messages.FinishedPipelineSender;
import co.edu.javeriana.hellsoft.cpplugins.model.User;
import co.edu.javeriana.hellsoft.cpplugins.model.pipeline.PipelineDescriptor;
import co.edu.javeriana.hellsoft.cpplugins.service.ExecutionState;
import co.edu.javeriana.hellsoft.cpplugins.service.core.PipelineService;
import co.edu.javeriana.hellsoft.cpplugins.service.core.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/cpPlugins/pipeline")
class PipelineController {

    @Autowired
    private PipelineService pipelineService;

    @Autowired
    private UserService userService;

    @GetMapping("")
    public Response getUserPipelines() {
        User user = getAuthenticatedUser();
        List<PipelineDto> userPipelines = pipelineService.findUserPipelines(user);
        return Response.ok().setPayload(userPipelines);
    }

    @GetMapping("/state/{executionState}")
    public Response getUserPipelinesByState(@PathVariable String executionState) {
        User user = getAuthenticatedUser();
        List<PipelineDto> userPipelines =
                pipelineService.findUserPipelinesByState(user, executionState);
        return Response.ok().setPayload(userPipelines);
    }

    @GetMapping("/{pipelineId}")
    public Response getPipelineById(@PathVariable String pipelineId) {
        User user = getAuthenticatedUser();
        PipelineDescriptor descriptor = pipelineService.findPipelineById(user, pipelineId);
        return Response.ok().setPayload(descriptor);
    }

    @PostMapping("/new")
    public Response newPipeline(@RequestBody @Valid NewPipelineRequest newPipelineRequest) {
        User user = getAuthenticatedUser();
        PipelineDto newPipeline =
                pipelineService.addPipeline(
                        user,
                        newPipelineRequest.getName(),
                        newPipelineRequest.getDescription(),
                        newPipelineRequest.getPipelineDesign());
        return Response.ok().setPayload(newPipeline);
    }

    @PutMapping("/update")
    public Response updatePipeline(@RequestBody @Valid UpdatePipelineRequest updatePipelineRequest) {
        User user = getAuthenticatedUser();
        PipelineDto newPipeline =
                pipelineService.updatePipeline(
                        user,
                        updatePipelineRequest.getPipelineName(),
                        updatePipelineRequest.getPipelineId(),
                        updatePipelineRequest.getPipelineDesign());
        return Response.ok().setPayload(newPipeline);
    }

    @DeleteMapping("/delete/{pipelineId}")
    public Response removePipeline(@PathVariable String pipelineId) {
        User user = getAuthenticatedUser();
        pipelineService.removePipeline(user, pipelineId);
        return Response.ok().setPayload("Pipeline removed");
    }

    @PostMapping("/execute")
    public Response executePipeline(
            @RequestBody @Valid ExecutePipelineRequest executePipelineRequest) {
        User user = getAuthenticatedUser();
        PipelineDescriptor descriptor =
                pipelineService.replaceCustomFilterToDescriptor(executePipelineRequest.getPipelineDesign());
        pipelineService.runPipeline(user, descriptor);
        return Response.ok().setPayload(descriptor);
    }

    @GetMapping("/stop/{pipelineId}")
    public Response stopPipelineExecution(@PathVariable String pipelineId) {
        User user = getAuthenticatedUser();
        pipelineService.changePipelineExecutionState(user, ExecutionState.STOPPED.state(), pipelineId);
        return Response.ok().setPayload("Pipeline has stopped successfully !");
    }

    @GetMapping("/check/{pipelineId}")
    public Response checkPipelineExecution(@PathVariable String pipelineId) {
        User user = getAuthenticatedUser();
        pipelineService.changePipelineExecutionState(
                user, ExecutionState.NOT_EXECUTING.state(), pipelineId);
        return Response.ok().setPayload("Pipeline has been checked successfully !");
    }

    @GetMapping("/cancel/{pipelineId}")
    public Response cancelPipelineExecution(@PathVariable String pipelineId) {
        User user = getAuthenticatedUser();
        pipelineService.changePipelineExecutionState(
                user, ExecutionState.CANCELLED.state(), pipelineId);
        return Response.ok().setPayload("Pipeline has been cancelled successfully !");
    }

    @GetMapping("/resume/{pipelineId}")
    public Response resumePipelineExecution(@PathVariable String pipelineId) {
        User user = getAuthenticatedUser();
        pipelineService.changePipelineExecutionState(
                user, ExecutionState.EXECUTING.state(), pipelineId);
        return Response.ok().setPayload("Pipeline has been resumed successfully !");
    }

    private User getAuthenticatedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return userService.findNativeUserByUserName((String) auth.getPrincipal());
    }
}
