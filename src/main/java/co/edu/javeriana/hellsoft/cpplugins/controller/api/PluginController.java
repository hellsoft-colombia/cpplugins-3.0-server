package co.edu.javeriana.hellsoft.cpplugins.controller.api;

import co.edu.javeriana.hellsoft.cpplugins.dto.model.CustomPluginDto;
import co.edu.javeriana.hellsoft.cpplugins.dto.response.Response;
import co.edu.javeriana.hellsoft.cpplugins.model.User;
import co.edu.javeriana.hellsoft.cpplugins.model.customplugin.CustomNodeIO;
import co.edu.javeriana.hellsoft.cpplugins.service.core.CustomPluginService;
import co.edu.javeriana.hellsoft.cpplugins.service.core.PluginService;
import co.edu.javeriana.hellsoft.cpplugins.service.core.UserService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/cpPlugins/")
class PluginController {

    @Autowired
    private PluginService pluginService;

    @Autowired
    private CustomPluginService customPluginService;

    @Autowired
    private UserService userService;

    @GetMapping(value = "/plugins", produces = "application/json")
    public Response getFiltersList() throws IOException {
        String pluginsJson = pluginService.getSystemPlugins();
        return Response.ok().setPayload(JsonLoader.fromString("{" + pluginsJson + "}"));
    }

    @GetMapping(value = "/plugins/all", produces = "application/json")
    public Response getAllUserfilterList() throws IOException {
        User user = getAuthenticatedUser();
        String plugins = pluginService.getSystemPlugins();
        JsonNode cpPluginJson = JsonLoader.fromString( plugins );
        List<CustomPluginDto> userPipelines = customPluginService.getUserCustomPlugins(user);
        ArrayNode pluginsJson;
        ObjectMapper mapper = new ObjectMapper();

        if (userPipelines.isEmpty())
            return Response.ok().setPayload(cpPluginJson);
        else {
            pluginsJson = (ArrayNode) cpPluginJson.get("functionList");
            ObjectNode customNodeInfo = mapper.createObjectNode();

            customNodeInfo.put("title", "Custom");
            customNodeInfo.put("subtitle", "Paquete personalizado");
            customNodeInfo.put("isDirectory", true);
            customNodeInfo.put("isCustom", true);

            ArrayNode customNodeArray = mapper.createArrayNode();
            for (CustomPluginDto customPlugin : userPipelines) {
                ObjectNode customPluginNode = mapper.createObjectNode();

                customPluginNode.put("isCustom", true);
                customPluginNode.put("pipelineId", customPlugin.getFileId());
                customPluginNode.put("title", customPlugin.getFileName());
                customPluginNode.put("type", customPlugin.getType());
                customPluginNode.put("nodeClass", customPlugin.getNodeClass());
                customPluginNode.put("subtitle", customPlugin.getDescription());

                ArrayNode inputsArray = mapper.createArrayNode();
                for (CustomNodeIO input : customPlugin.getInputs()) {
                    ObjectNode inputNode = getJsonObjectFromNodeIO(mapper, input);
                    inputsArray.add(inputNode);
                }
                customPluginNode.putPOJO("inputs", inputsArray);

                ArrayNode outputsArray = mapper.createArrayNode();
                for (CustomNodeIO output : customPlugin.getOutputs()) {
                    ObjectNode outputNode = getJsonObjectFromNodeIO(mapper, output);
                    outputsArray.add(outputNode);
                }
                customPluginNode.putPOJO("outputs", outputsArray);

                customNodeArray.add(customPluginNode);
            }

            customNodeInfo.putPOJO("children", customNodeArray);
            pluginsJson.add(customNodeInfo);

            ObjectNode resultJson = mapper.createObjectNode();
            resultJson.putPOJO("functionList", pluginsJson);
            return Response.ok().setPayload(resultJson);
        }
    }

    private ObjectNode getJsonObjectFromNodeIO(ObjectMapper mapper, CustomNodeIO nodeIO) {
        ObjectNode node = mapper.createObjectNode();
        ObjectNode referenceNode = mapper.createObjectNode();

        node.put("title", nodeIO.getName());
        referenceNode.put("nodeId", nodeIO.getReference().getNodeId());
        referenceNode.put("ioId", nodeIO.getReference().getIoId());
        node.putPOJO("reference", referenceNode);

        return node;
    }

    private User getAuthenticatedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return userService.findNativeUserByUserName((String) auth.getPrincipal());
    }
}
