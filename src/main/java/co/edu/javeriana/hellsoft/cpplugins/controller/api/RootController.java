package co.edu.javeriana.hellsoft.cpplugins.controller.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
class RootController {
    @GetMapping
    public @ResponseBody
    String main() {
        return "This is not our root endpoint. see /cpPLugins/";
    }
}
