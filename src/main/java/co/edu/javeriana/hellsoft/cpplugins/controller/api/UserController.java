package co.edu.javeriana.hellsoft.cpplugins.controller.api;

import co.edu.javeriana.hellsoft.cpplugins.controller.request.UserRegisterRequest;
import co.edu.javeriana.hellsoft.cpplugins.dto.mapper.UserMapper;
import co.edu.javeriana.hellsoft.cpplugins.dto.model.UserDto;
import co.edu.javeriana.hellsoft.cpplugins.dto.response.Response;
import co.edu.javeriana.hellsoft.cpplugins.model.User;
import co.edu.javeriana.hellsoft.cpplugins.service.core.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cpPlugins/user")
class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/info")
    public Response getUserInfo() {
        User user = getAuthenticatedUser();
        return Response.ok().setPayload(UserMapper.toUserDto(user));
    }

    @PostMapping("/signup")
    public Response signup(@RequestBody @Valid UserRegisterRequest userRegisterRequest) {
        return Response.ok().setPayload(registerUser(userRegisterRequest));
    }

    private UserDto registerUser(UserRegisterRequest userRegisterRequest) {
        UserDto userDto =
                new UserDto()
                        .setEmail(userRegisterRequest.getEmail())
                        .setFirstName(userRegisterRequest.getFirstName())
                        .setLastName(userRegisterRequest.getLastName())
                        .setPassword(userRegisterRequest.getPassword())
                        .setUserName(userRegisterRequest.getUserName());

        return userService.signUp(userDto);
    }

    private User getAuthenticatedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return userService.findNativeUserByUserName((String) auth.getPrincipal());
    }
}
