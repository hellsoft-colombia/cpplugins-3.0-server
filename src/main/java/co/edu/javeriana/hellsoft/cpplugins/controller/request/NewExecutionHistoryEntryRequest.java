package co.edu.javeriana.hellsoft.cpplugins.controller.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class NewExecutionHistoryEntryRequest {
    @NotEmpty(message = "{constraints.NotEmpty}")
    String executionState;

    @NotEmpty(message = "{constraints.NotEmpty}")
    String pipelineIdReference;

    @NotEmpty(message = "{constraints.NotEmpty}")
    String pipelineNameReference;

    @NotEmpty(message = "{constraints.NotEmpty}")
    String pipelineDescriptionReference;

    @NotEmpty(message = "{constraints.NotEmpty}")
    String executionResult;
}
