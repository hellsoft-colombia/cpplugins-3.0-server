package co.edu.javeriana.hellsoft.cpplugins.controller.request;

import co.edu.javeriana.hellsoft.cpplugins.model.pipeline.PipelineDescriptor;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class NewPipelineRequest {
    @NotEmpty(message = "{constraints.NotEmpty}")
    private String name;

    @NotEmpty(message = "{constraints.NotEmpty}")
    private String description;

    @NotNull
    private PipelineDescriptor pipelineDesign;
}
