package co.edu.javeriana.hellsoft.cpplugins.controller.request;

import co.edu.javeriana.hellsoft.cpplugins.model.customplugin.CustomNodeIO;
import co.edu.javeriana.hellsoft.cpplugins.model.pipeline.PipelineDescriptor;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateCustomPluginRequest {
    @NotEmpty(message = "{constraints.NotEmpty}")
    private String pluginId;

    @NotEmpty(message = "{constraints.NotEmpty}")
    private String pluginName;

    @NotNull(message = "{constraints.NotEmpty}")
    private String subtitle;

    @NotNull(message = "{constraints.NotEmpty}")
    private List<CustomNodeIO> inputs;

    @NotNull(message = "{constraints.NotEmpty}")
    private List<CustomNodeIO> outputs;

    @NotNull
    private PipelineDescriptor descriptor;
}
