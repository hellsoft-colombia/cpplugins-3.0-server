package co.edu.javeriana.hellsoft.cpplugins.controller.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserRegisterRequest {
    @NotEmpty(message = "{constraints.NotEmpty}")
    private String email;

    @NotEmpty(message = "{constraints.NotEmpty}")
    private String password;

    @NotEmpty(message = "{constraints.NotEmpty}")
    private String firstName;

    @NotEmpty(message = "{constraints.NotEmpty}")
    private String lastName;

    @NotEmpty(message = "{constraints.NotEmpty}")
    private String userName;
}
