package co.edu.javeriana.hellsoft.cpplugins.dto.jni;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
public class PipelineExecutionDto {
    public boolean executionState;
    public String executionMessage;
    public int expectedTime;
}
