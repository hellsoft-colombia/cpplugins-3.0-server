package co.edu.javeriana.hellsoft.cpplugins.dto.mapper;

import co.edu.javeriana.hellsoft.cpplugins.dto.model.CustomPluginDto;
import co.edu.javeriana.hellsoft.cpplugins.model.customplugin.CustomPlugin;

public class CustomPluginMapper {
    public static CustomPluginDto toCustomPluginDto(CustomPlugin plugin) {
        return new CustomPluginDto()
                .setInputs(plugin.getInputs())
                .setOutputs(plugin.getOutputs())
                .setFileName(plugin.getFileName())
                .setDescription(plugin.getSubtitle())
                .setCreationDate(plugin.getCreationDate())
                .setLastModificationDate(plugin.getLastModificationDate())
                .setFileId(plugin.getId())
                .setType(plugin.getType())
                .setNodeClass(plugin.getNodeClass());
    }
}
