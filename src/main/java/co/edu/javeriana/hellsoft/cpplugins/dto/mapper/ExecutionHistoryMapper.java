package co.edu.javeriana.hellsoft.cpplugins.dto.mapper;

import co.edu.javeriana.hellsoft.cpplugins.dto.model.ExecutionHistoryDto;
import co.edu.javeriana.hellsoft.cpplugins.model.ExecutionHistory;

public class ExecutionHistoryMapper {
    public static ExecutionHistoryDto toExecutionHistoryDto(ExecutionHistory executionHistory) {
        return new ExecutionHistoryDto()
                .setExecutionResult(executionHistory.getExecutionResult())
                .setLastExecutionTime(executionHistory.getExecutionTime())
                .setId(executionHistory.getId())
                .setFileId(executionHistory.getPipelineIdReference())
                .setFileName(executionHistory.getPipelineNameReference())
                .setExecutionState(executionHistory.getState())
                .setImageResults(executionHistory.getImageResults())
                .setDescription(executionHistory.getPipelineDescriptionReference());
    }
}
