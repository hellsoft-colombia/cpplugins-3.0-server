package co.edu.javeriana.hellsoft.cpplugins.dto.mapper;

import co.edu.javeriana.hellsoft.cpplugins.dto.model.ImageDto;
import co.edu.javeriana.hellsoft.cpplugins.model.Image;

public class ImageMapper {
    public static ImageDto toImageDto(Image image) {
        return new ImageDto()
                .setFileId(image.getId())
                .setFormat(image.getFormat())
                .setSize(image.getImageSize())
                .setCreationDate(image.getCreationDate())
                .setFileName(image.getFileName());
    }
}
