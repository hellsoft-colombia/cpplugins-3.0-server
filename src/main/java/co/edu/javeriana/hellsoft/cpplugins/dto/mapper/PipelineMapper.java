package co.edu.javeriana.hellsoft.cpplugins.dto.mapper;

import co.edu.javeriana.hellsoft.cpplugins.dto.model.PipelineDto;
import co.edu.javeriana.hellsoft.cpplugins.model.Pipeline;

public class PipelineMapper {
    public static PipelineDto toPipelineDto(Pipeline pipeline) {
        return new PipelineDto()
                .setDescription(pipeline.getDescription())
                .setCreationDate(pipeline.getCreationDate())
                .setExecutionState(pipeline.getState())
                .setFileId(pipeline.getId())
                .setFileName(pipeline.getFileName())
                .setLastModificationDate(pipeline.getLastModificationDate())
                .setLastExecutionTime(pipeline.getLastExecutionTime());
    }
}
