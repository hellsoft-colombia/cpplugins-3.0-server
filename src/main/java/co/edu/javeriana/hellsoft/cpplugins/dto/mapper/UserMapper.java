package co.edu.javeriana.hellsoft.cpplugins.dto.mapper;

import co.edu.javeriana.hellsoft.cpplugins.dto.model.UserDto;
import co.edu.javeriana.hellsoft.cpplugins.model.User;

public class UserMapper {
    public static UserDto toUserDto(User user) {
        return new UserDto()
                .setEmail(user.getEmail())
                .setFirstName(user.getFirstName())
                .setLastName(user.getLastName())
                .setUserName(user.getUserName());
    }
}
