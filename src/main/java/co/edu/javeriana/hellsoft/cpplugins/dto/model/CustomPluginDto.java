package co.edu.javeriana.hellsoft.cpplugins.dto.model;

import co.edu.javeriana.hellsoft.cpplugins.model.customplugin.CustomNodeIO;
import co.edu.javeriana.hellsoft.cpplugins.model.pipeline.PipelineDescriptor;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomPluginDto {
    private String fileId;
    private String fileName;
    private String type;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date creationDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private Date lastModificationDate;

    private String nodeClass;
    private String description;
    private PipelineDescriptor descriptorDTO;
    private List<CustomNodeIO> inputs;
    private List<CustomNodeIO> outputs;
}
