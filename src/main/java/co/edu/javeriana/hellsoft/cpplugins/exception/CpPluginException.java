package co.edu.javeriana.hellsoft.cpplugins.exception;

import co.edu.javeriana.hellsoft.cpplugins.config.cpPluginsConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.Optional;

@Component
public class CpPluginException {

    private static cpPluginsConfig propertiesConfig;

    @Autowired
    public CpPluginException(cpPluginsConfig propertiesConfig) {
        CpPluginException.propertiesConfig = propertiesConfig;
    }

    public static RuntimeException throwException(String messageTemplate, String... args) {
        return new RuntimeException(format(messageTemplate, args));
    }

    public static RuntimeException throwException(
            EntityType entityType, ExceptionType exceptionType, String... args) {
        String messageTemplate = getMessageTemplate(entityType, exceptionType);
        return throwException(exceptionType, messageTemplate, args);
    }

    public static RuntimeException throwExceptionWithId(
            EntityType entityType, ExceptionType exceptionType, String id, String... args) {
        String messageTemplate = getMessageTemplate(entityType, exceptionType).concat(".").concat(id);
        return throwException(exceptionType, messageTemplate, args);
    }

    public static RuntimeException throwExceptionWithTemplate(
            EntityType entityType, ExceptionType exceptionType, String messageTemplate, String... args) {
        return throwException(exceptionType, messageTemplate, args);
    }

    private static RuntimeException throwException(
            ExceptionType exceptionType, String messageTemplate, String... args) {
        if (ExceptionType.BAD_FORMATTED_DATA.equals(exceptionType)) {
            return new BadFormattedDataException(format(messageTemplate, args));
        } else if (ExceptionType.DUPLICATE_ENTITY.equals(exceptionType)) {
            return new DuplicateEntityException(format(messageTemplate, args));
        } else if (ExceptionType.INTERNAL_SERVER_ERROR.equals(exceptionType)) {
            return new InternalServerErrorException(format(messageTemplate, args));
        } else if (ExceptionType.NOT_FOUND.equals(exceptionType)) {
            return new NotFoundException(format(messageTemplate, args));
        } else if (ExceptionType.UNAUTHORIZED.equals(exceptionType)) {
            return new UnauthorizedException(format(messageTemplate, args));
        } else if (ExceptionType.BAD_CREDENTIALS.equals(exceptionType)) {
            return new WrongCredentialsException(format(messageTemplate, args));
        }
        return new RuntimeException(format(messageTemplate, args));
    }

    private static String getMessageTemplate(EntityType entityType, ExceptionType exceptionType) {
        return entityType.name().concat(".").concat(exceptionType.getValue()).toLowerCase();
    }

    private static String format(String template, String... args) {
        Optional<String> templateContent =
                Optional.ofNullable(propertiesConfig.getConfigValue(template));
        return templateContent.map(s -> MessageFormat.format(s, args)).orElseGet(() -> String.format(template, args));
    }

    static class BadFormattedDataException extends RuntimeException {
        BadFormattedDataException(String message) {
            super(message);
        }
    }

    static class DuplicateEntityException extends RuntimeException {
        DuplicateEntityException(String message) {
            super(message);
        }
    }

    static class InternalServerErrorException extends RuntimeException {
        InternalServerErrorException(String message) {
            super(message);
        }
    }

    static class NotFoundException extends RuntimeException {
        NotFoundException(String message) {
            super(message);
        }
    }

    static class UnauthorizedException extends RuntimeException {
        UnauthorizedException(String message) {
            super(message);
        }
    }

    static class WrongCredentialsException extends RuntimeException {
        WrongCredentialsException(String message) {
            super(message);
        }
    }
}
