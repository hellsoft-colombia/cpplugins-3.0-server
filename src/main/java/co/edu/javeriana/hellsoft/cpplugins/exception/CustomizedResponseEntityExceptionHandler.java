package co.edu.javeriana.hellsoft.cpplugins.exception;

import co.edu.javeriana.hellsoft.cpplugins.dto.response.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@RestController
class CustomizedResponseEntityExceptionHandler {

    @ExceptionHandler(CpPluginException.BadFormattedDataException.class)
    public final ResponseEntity<? extends Response<?>> handleBadFormattedDataException(Exception ex, WebRequest request) {
        Response<?> response = Response.badFormattedData();
        response.addErrorMsgToResponse(ex.getMessage(), ex);
        return new ResponseEntity<Response<?>>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(com.mongodb.MongoException.class)
    public final ResponseEntity<? extends Response<?>> handleDatabaseException(Exception ex, WebRequest request) {
        Response<?> response = Response.duplicateEntity();
        response.addErrorMsgToResponse(ex.getMessage(), ex);
        return new ResponseEntity<Response<?>>(response, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(io.jsonwebtoken.security.SignatureException.class)
    public final ResponseEntity<? extends Response<?>> handleBadFormattedToken(Exception ex, WebRequest request) {
        Response<?> response = Response.unauthorized();
        response.addErrorMsgToResponse(ex.getMessage(), ex);
        return new ResponseEntity<Response<?>>(response, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(io.jsonwebtoken.security.SecurityException.class)
    public final ResponseEntity<? extends Response<?>> handleSecurityIssues(Exception ex, WebRequest request) {
        Response<?> response = Response.unauthorized();
        response.addErrorMsgToResponse(ex.getMessage(), ex);
        return new ResponseEntity<Response<?>>(response, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(CpPluginException.UnauthorizedException.class)
    public final ResponseEntity<? extends Response<?>> handleUnauthorizedException(Exception ex, WebRequest request) {
        Response<?> response = Response.unauthorized();
        response.addErrorMsgToResponse(ex.getMessage(), ex);
        return new ResponseEntity<Response<?>>(response, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(CpPluginException.DuplicateEntityException.class)
    public final ResponseEntity<? extends Response<?>> handleDuplicateEntityException(Exception ex, WebRequest request) {
        Response<?> response = Response.duplicateEntity();
        response.addErrorMsgToResponse(ex.getMessage(), ex);
        return new ResponseEntity<Response<?>>(response, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(CpPluginException.InternalServerErrorException.class)
    public final ResponseEntity<? extends Response<?>> handleInternalServerErrorException(Exception ex, WebRequest request) {
        Response<?> response = Response.internalServerError();
        response.addErrorMsgToResponse(ex.getMessage(), ex);
        return new ResponseEntity<Response<?>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(CpPluginException.NotFoundException.class)
    public final ResponseEntity<? extends Response<?>> handleNotFoundException(Exception ex, WebRequest request) {
        Response<?> response = Response.notFound();
        response.addErrorMsgToResponse(ex.getMessage(), ex);
        return new ResponseEntity<Response<?>>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CpPluginException.WrongCredentialsException.class)
    public final ResponseEntity<? extends Response<?>> handleWrongCredentialsException(Exception ex, WebRequest request) {
        Response<?> response = Response.wrongCredentials();
        response.addErrorMsgToResponse(ex.getMessage(), ex);
        return new ResponseEntity<Response<?>>(response, HttpStatus.NOT_ACCEPTABLE);
    }
}
