package co.edu.javeriana.hellsoft.cpplugins.exception;

public enum EntityType {
    CUSTOM_PLUGIN,
    IMAGE,
    PIPELINE,
    USER,
    HISTORY_REGISTER
}
