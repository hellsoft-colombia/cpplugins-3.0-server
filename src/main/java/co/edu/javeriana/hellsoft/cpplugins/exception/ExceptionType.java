package co.edu.javeriana.hellsoft.cpplugins.exception;

public enum ExceptionType {
    BAD_FORMATTED_DATA("bad.formatted.data"),
    DUPLICATE_ENTITY("duplicate"),
    INTERNAL_SERVER_ERROR("internal.server.error"),
    NOT_FOUND("not.found"),
    UNAUTHORIZED("not.authorized"),
    BAD_CREDENTIALS("bad.credentials");

    private final String value;

    ExceptionType(String value) {
        this.value = value;
    }

    String getValue() {
        return this.value;
    }
}
