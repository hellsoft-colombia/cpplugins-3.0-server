package co.edu.javeriana.hellsoft.cpplugins.messages;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@NoArgsConstructor
public class FinishedPipeline implements Serializable {
    private String pipelineId;
    private String result;
    private double elapsedTime;
    private String folderResult;
}
