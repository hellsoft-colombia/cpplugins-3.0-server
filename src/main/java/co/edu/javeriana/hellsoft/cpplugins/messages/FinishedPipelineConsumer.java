package co.edu.javeriana.hellsoft.cpplugins.messages;

import co.edu.javeriana.hellsoft.cpplugins.service.core.PipelineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import static co.edu.javeriana.hellsoft.cpplugins.config.ActiveMQConfig.PLUGINS_QUEUE;

@Component
class FinishedPipelineConsumer {
    private static final Logger log = LoggerFactory.getLogger(FinishedPipelineConsumer.class);

    @Autowired
    private PipelineService pipelineService;
    /*
    @JmsListener(destination = PLUGINS_QUEUE)
    public void receiveMessage(@Payload FinishedPipeline finishedPipeline,
                               @Headers MessageHeaders headers,
                               Message message, Session session) {
        log.info("received <" + finishedPipeline.toString() + ">");

        log.info("- - - - - - - - - - - - - - - - - - - - - - - -");
        log.info("######          Message Details           #####");
        log.info("- - - - - - - - - - - - - - - - - - - - - - - -");
        log.info("headers: " + headers);
        log.info("message: " + message);
        log.info("session: " + session);
        log.info("- - - - - - - - - - - - - - - - - - - - - - - -");
    }*/

    @JmsListener(destination = PLUGINS_QUEUE)
    public void handleFinishedPipeline(@Payload FinishedPipeline finishedPipeline) {
        System.out.println("============  NEW MESSAGE ============");
        pipelineService.handleFinishedPipeline(finishedPipeline);
        System.out.println("============  END  ============");
    }
}
