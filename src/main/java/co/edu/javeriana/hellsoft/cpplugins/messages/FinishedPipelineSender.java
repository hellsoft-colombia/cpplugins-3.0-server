package co.edu.javeriana.hellsoft.cpplugins.messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import static co.edu.javeriana.hellsoft.cpplugins.config.ActiveMQConfig.PLUGINS_QUEUE;

@Service
public
class FinishedPipelineSender {
    private static final Logger log = LoggerFactory.getLogger(FinishedPipelineSender.class);

    @Autowired
    private JmsTemplate jmsTemplate;

    public void send(FinishedPipeline myMessage) {

        System.out.println("sending with convertAndSend() to queue <" + myMessage + ">");
        jmsTemplate.convertAndSend(PLUGINS_QUEUE, myMessage);
    }
}
