package co.edu.javeriana.hellsoft.cpplugins.model;

import co.edu.javeriana.hellsoft.cpplugins.dto.model.ImageDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@Document(collection = "execution_history")
public class ExecutionHistory {
    @Id
    private String id;

    @NotEmpty
    @NotNull
    private String state;

    @NotNull
    private Date executionTime;

    @NotNull
    private String pipelineIdReference;

    @NotNull
    private String pipelineNameReference;

    @NotNull
    private String pipelineDescriptionReference;

    private String executionResult;

    @Indexed(direction = IndexDirection.ASCENDING)
    private List<ImageDto> imageResults;

    @NotNull
    @DBRef(lazy = true, db = "user")
    private User user;
}
