package co.edu.javeriana.hellsoft.cpplugins.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@Document(collection = "image")
public class Image {

    @Id
    private String id;

    @NotEmpty
    @NotNull
    @Indexed(unique = true, direction = IndexDirection.DESCENDING)
    private String fileName;

    @NotNull
    private Date creationDate;

    @NotNull
    private float ImageSize;

    @NotNull
    private String format;

    @NotEmpty
    @NotNull
    private String path;

    @NotNull
    @DBRef(lazy = true, db = "user")
    private User user;
}
