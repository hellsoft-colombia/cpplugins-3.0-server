package co.edu.javeriana.hellsoft.cpplugins.model;

import co.edu.javeriana.hellsoft.cpplugins.model.pipeline.PipelineDescriptor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@Document(collection = "pipeline")
public class Pipeline {

    @Id
    private String id;

    @NotEmpty
    @NotNull
    private String state;

    @NotEmpty
    @NotNull
    @Indexed(unique = true, direction = IndexDirection.DESCENDING)
    private String fileName;

    private String description;

    @NotNull
    private Date creationDate;

    private Date lastExecutionTime;

    @NotNull
    private Date LastModificationDate;

    private PipelineDescriptor pipelineDescriptor;

    @NotNull
    @DBRef(lazy = true, db = "user")
    private User user;
}
