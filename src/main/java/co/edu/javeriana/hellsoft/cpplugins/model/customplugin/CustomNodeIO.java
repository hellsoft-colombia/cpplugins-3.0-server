package co.edu.javeriana.hellsoft.cpplugins.model.customplugin;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
@NoArgsConstructor
public class CustomNodeIO {

    @NotEmpty @NotNull ReferenceIO reference;
    @NotEmpty
    @NotNull
    private String name;
}
