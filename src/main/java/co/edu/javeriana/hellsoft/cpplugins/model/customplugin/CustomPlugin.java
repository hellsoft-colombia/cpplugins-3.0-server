package co.edu.javeriana.hellsoft.cpplugins.model.customplugin;

import co.edu.javeriana.hellsoft.cpplugins.model.User;
import co.edu.javeriana.hellsoft.cpplugins.model.pipeline.PipelineDescriptor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@Document(collection = "custom_plugin")
public class CustomPlugin {

    @Id
    private String id;

    @NotEmpty
    @NotNull
    private String fileName;

    private String type;

    private String nodeClass;

    private String subtitle;

    @NotNull
    private Date creationDate;

    @NotNull
    private Date LastModificationDate;

    private List<CustomNodeIO> inputs;

    private List<CustomNodeIO> outputs;

    private PipelineDescriptor pluginDescriptor;

    @NotNull
    @DBRef(lazy = true, db = "user")
    private User user;
}
