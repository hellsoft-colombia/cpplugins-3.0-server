package co.edu.javeriana.hellsoft.cpplugins.model.customplugin;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
@NoArgsConstructor
public class ReferenceIO {
    @NotEmpty
    @NotNull
    private int nodeId;
    @NotEmpty
    @NotNull
    private String ioId;
}
