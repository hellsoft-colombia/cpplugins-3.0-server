package co.edu.javeriana.hellsoft.cpplugins.model.pipeline;

import co.edu.javeriana.hellsoft.cpplugins.model.pipeline.connection.Connection;
import co.edu.javeriana.hellsoft.cpplugins.model.pipeline.node.Node;
import co.edu.javeriana.hellsoft.cpplugins.model.pipeline.parameter.Parameter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Set;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PipelineDescriptor {
    private String pipelineId;
    private int executionNodeId;
    private long timeStamp;
    private Set<Node> nodes;
    private Set<Connection> connections;
    private Set<Parameter> parameters;
}
