package co.edu.javeriana.hellsoft.cpplugins.model.pipeline.node;

import co.edu.javeriana.hellsoft.cpplugins.model.pipeline.PipelineDescriptor;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.ArrayList;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Node {
    private String pluginId;
    private int executionNodeId;
    private long timeStamp;

    private boolean descriptor;
    private String title;
    private int nodeId;
    private String d2dId;
    private String nodeClass;
    private String type;
    private float x;
    private float y;
    private ArrayList<String> parameters;
    private ArrayList<Object> inputs;
    private ArrayList<Object> outputs;
    private PipelineDescriptor descriptorDesign;
}
