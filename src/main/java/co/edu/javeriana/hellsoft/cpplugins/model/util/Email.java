package co.edu.javeriana.hellsoft.cpplugins.model.util;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
public class Email {
    private String to;
    private String subject;
    private String body;
}
