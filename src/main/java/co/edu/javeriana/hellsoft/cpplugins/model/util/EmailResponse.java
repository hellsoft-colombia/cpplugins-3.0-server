package co.edu.javeriana.hellsoft.cpplugins.model.util;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
public class EmailResponse {
    public static final String SUCCESS = "SUCCESS";
    public static final String ERROR = "ERROR";

    private String status;
    private String errorMessage;

    public static EmailResponse success() {
        return new EmailResponse().setStatus(EmailResponse.SUCCESS);
    }

    public static EmailResponse error(String errorMessage) {
        return new EmailResponse().setStatus(EmailResponse.ERROR).setErrorMessage(errorMessage);
    }

    public boolean isSuccess() {
        return SUCCESS.equals(this.status);
    }

    public boolean isError() {
        return ERROR.equals(this.status);
    }
}
