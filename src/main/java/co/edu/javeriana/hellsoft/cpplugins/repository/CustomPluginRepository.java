package co.edu.javeriana.hellsoft.cpplugins.repository;

import co.edu.javeriana.hellsoft.cpplugins.model.User;
import co.edu.javeriana.hellsoft.cpplugins.model.customplugin.CustomPlugin;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface CustomPluginRepository extends MongoRepository<CustomPlugin, String> {
    Set<CustomPlugin> findCustomPluginsByUser(User user);
}
