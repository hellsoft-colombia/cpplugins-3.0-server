package co.edu.javeriana.hellsoft.cpplugins.repository;

import co.edu.javeriana.hellsoft.cpplugins.model.ExecutionHistory;
import co.edu.javeriana.hellsoft.cpplugins.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface ExecutionHistoryRepository
        extends PagingAndSortingRepository<ExecutionHistory, String> {
    Page<ExecutionHistory> findByUser(User user, Pageable pageable);

    Long countByUser(User user);

    Page<ExecutionHistory> findByUserAndState(User user, String state, Pageable pageable);

    Long countByUserAndState(User user, String state);

    Optional<ExecutionHistory> findByUserAndId(User user, String id);
}
