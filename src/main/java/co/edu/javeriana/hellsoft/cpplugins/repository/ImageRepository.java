package co.edu.javeriana.hellsoft.cpplugins.repository;

import co.edu.javeriana.hellsoft.cpplugins.model.Image;
import co.edu.javeriana.hellsoft.cpplugins.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface ImageRepository extends MongoRepository<Image, String> {
    Image findByFileName(String fileName);

    Set<Image> findImagesByUser(User user);
}
