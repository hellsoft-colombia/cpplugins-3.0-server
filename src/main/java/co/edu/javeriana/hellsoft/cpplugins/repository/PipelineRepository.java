package co.edu.javeriana.hellsoft.cpplugins.repository;

import co.edu.javeriana.hellsoft.cpplugins.model.Pipeline;
import co.edu.javeriana.hellsoft.cpplugins.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface PipelineRepository extends MongoRepository<Pipeline, String> {
    Set<Pipeline> findPipelinesByUser(User user);
}
