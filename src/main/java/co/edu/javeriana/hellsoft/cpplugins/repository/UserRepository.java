package co.edu.javeriana.hellsoft.cpplugins.repository;

import co.edu.javeriana.hellsoft.cpplugins.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
    User findUserByUserNameEquals(String userName);
}
