package co.edu.javeriana.hellsoft.cpplugins.security;

import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static co.edu.javeriana.hellsoft.cpplugins.security.SecurityConstants.*;

public class CookieUtil {
    public static void create(HttpServletResponse httpServletResponse, String value) {
        Cookie cookie = new Cookie(COOKIE_NAME, value);
        cookie.setSecure(SECURE_COOKIE);
        cookie.setHttpOnly(true);
        cookie.setMaxAge((int) EXPIRATION_TIME / 1000);
        cookie.setPath(BASE_PATH);
        httpServletResponse.addCookie(cookie);
    }

    public static void clear(HttpServletResponse httpServletResponse) {
        Cookie cookie = new Cookie(COOKIE_NAME, null);
        cookie.setPath(BASE_PATH);
        cookie.setHttpOnly(true);
        cookie.setMaxAge(0);
        httpServletResponse.addCookie(cookie);
    }

    public static String getValue(HttpServletRequest req) {
        Cookie cookie = WebUtils.getCookie(req, COOKIE_NAME);
        return cookie != null ? cookie.getValue() : null;
    }
}
