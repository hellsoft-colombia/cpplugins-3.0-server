package co.edu.javeriana.hellsoft.cpplugins.security;

import co.edu.javeriana.hellsoft.cpplugins.dto.model.UserDto;
import co.edu.javeriana.hellsoft.cpplugins.service.core.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserDto userDto = userService.findUserByUserName(s);
        if (userDto != null) {
            return new org.springframework.security.core.userdetails.User(
                    userDto.getUserName(), userDto.getPassword(), new ArrayList<>());
        }
        throw new UsernameNotFoundException("User " + s + " does not exist.");
    }
}
