package co.edu.javeriana.hellsoft.cpplugins.security;

import lombok.Getter;

@Getter
public class SecurityConstants {
    public static String SECRET =
            "X4su9rJaM3uPdfYywTgaic0r3P9bZKQLUjMREWdTw5ixQlcXqs3zPFdV0w6T7GRRc1gXxc4Cu3fQ9tbASNNjAajThD9bv9RhZZEPM5WqoZSohfi795SBqgYEGFFxn1O9bKO4vpqn43X6Jvp5mkwOtx6WB9yOinICWB9Gcd1FNoKpBV4QaPawryIuBghlgQFbfwj9auLAlXhHrztM80JfmKMCYLtPAmhnsLE8hPcrATNlUnq3eS8FIaf4xHGKzdXrfG9GncYce7vhOQolwjRbQwhIfewhftQPm2oywmipkOGhneeuOtw56hbZFP7bMfVfW0ld8CIz9p2E2QQ";
    public static String TOKEN_PREFIX = "Bearer";
    public static long EXPIRATION_TIME = 864000000; // 10 days
    public static String BASE_PATH = "/";
    public static String BASE_DOMAIN = "localhost";
    public static String COOKIE_NAME = "cpplugins_authorization";
    public static boolean SECURE_COOKIE = false; // Change on production (to disallow http)
}
