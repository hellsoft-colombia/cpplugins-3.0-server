package co.edu.javeriana.hellsoft.cpplugins.security.api;

import co.edu.javeriana.hellsoft.cpplugins.exception.CpPluginException;
import co.edu.javeriana.hellsoft.cpplugins.security.CookieUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import static co.edu.javeriana.hellsoft.cpplugins.exception.EntityType.USER;
import static co.edu.javeriana.hellsoft.cpplugins.exception.ExceptionType.UNAUTHORIZED;
import static co.edu.javeriana.hellsoft.cpplugins.security.SecurityConstants.SECRET;
import static co.edu.javeriana.hellsoft.cpplugins.security.SecurityConstants.TOKEN_PREFIX;

public class ApiJWTAuthorizationFilter extends BasicAuthenticationFilter {

    public ApiJWTAuthorizationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest req, HttpServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        String token = CookieUtil.getValue(req);
        if (token == null || !token.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request)
            throws io.jsonwebtoken.security.SignatureException {
        String token = CookieUtil.getValue(request);
        if (token != null) {
            Claims claims =
                    Jwts.parser()
                            .setSigningKey(SECRET.getBytes(StandardCharsets.UTF_8))
                            .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                            .getBody();

            String user = claims.getSubject();
            ArrayList<GrantedAuthority> list = new ArrayList<>();
            if (user != null) {
                return new UsernamePasswordAuthenticationToken(user, null, list);
            }
        }
        throw CpPluginException.throwException(USER, UNAUTHORIZED, "Unauthorized");
    }
}
