package co.edu.javeriana.hellsoft.cpplugins.security.api;

import co.edu.javeriana.hellsoft.cpplugins.security.CookieUtil;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JwtLogOutFilter implements LogoutSuccessHandler {
    @Override
    public void onLogoutSuccess(
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse,
            Authentication authentication) {
        CookieUtil.clear(httpServletResponse);
    }
}
