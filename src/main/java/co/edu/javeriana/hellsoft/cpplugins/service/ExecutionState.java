package co.edu.javeriana.hellsoft.cpplugins.service;

public enum ExecutionState {
    STOPPED("STOPPED"),
    CANCELLED("CANCELLED"),
    EXECUTING("EXECUTING"),
    EXPIRED("EXPIRED"),
    WAITING_FOR_INPUT("WAITING_FOR_INPUT"),
    NOT_EXECUTING("NOT_EXECUTING"),
    FINISHED("FINISHED");

    private final String state;

    ExecutionState(String state) {
        this.state = state;
    }

    public String state() {
        return state;
    }
}
