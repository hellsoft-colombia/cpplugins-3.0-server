package co.edu.javeriana.hellsoft.cpplugins.service.core;

import co.edu.javeriana.hellsoft.cpplugins.dto.model.CustomPluginDto;
import co.edu.javeriana.hellsoft.cpplugins.model.User;
import co.edu.javeriana.hellsoft.cpplugins.model.customplugin.CustomNodeIO;
import co.edu.javeriana.hellsoft.cpplugins.model.pipeline.PipelineDescriptor;

import java.util.List;

public interface CustomPluginService {
    List<CustomPluginDto> getUserCustomPlugins(User user);

    PipelineDescriptor findCustomPluginById(User user, String pluginId);

    CustomPluginDto addCustomPlugin(
            User user,
            String pluginName,
            PipelineDescriptor descriptor,
            List<CustomNodeIO> inputs,
            List<CustomNodeIO> outputs,
            String subtitle);

    CustomPluginDto updateCustomPlugin(
            User user,
            String pluginId,
            String pluginName,
            PipelineDescriptor descriptor,
            List<CustomNodeIO> inputs,
            List<CustomNodeIO> outputs,
            String subtitle);

    void removeCustomPlugin(User user, String customPluginId);
}
