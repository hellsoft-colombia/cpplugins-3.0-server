package co.edu.javeriana.hellsoft.cpplugins.service.core;

import co.edu.javeriana.hellsoft.cpplugins.dto.mapper.CustomPluginMapper;
import co.edu.javeriana.hellsoft.cpplugins.dto.model.CustomPluginDto;
import co.edu.javeriana.hellsoft.cpplugins.exception.CpPluginException;
import co.edu.javeriana.hellsoft.cpplugins.model.User;
import co.edu.javeriana.hellsoft.cpplugins.model.customplugin.CustomNodeIO;
import co.edu.javeriana.hellsoft.cpplugins.model.customplugin.CustomPlugin;
import co.edu.javeriana.hellsoft.cpplugins.model.pipeline.PipelineDescriptor;
import co.edu.javeriana.hellsoft.cpplugins.repository.CustomPluginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static co.edu.javeriana.hellsoft.cpplugins.exception.EntityType.CUSTOM_PLUGIN;
import static co.edu.javeriana.hellsoft.cpplugins.exception.ExceptionType.DUPLICATE_ENTITY;
import static co.edu.javeriana.hellsoft.cpplugins.exception.ExceptionType.NOT_FOUND;

@Component
public class CustomPluginServiceImpl implements CustomPluginService {

    @Autowired
    private CustomPluginRepository customPluginRepository;

    @Override
    public List<CustomPluginDto> getUserCustomPlugins(User user) {
        List<CustomPluginDto> plugins = new ArrayList<>();
        for (CustomPlugin plugin : customPluginRepository.findCustomPluginsByUser(user)) {
            plugins.add(CustomPluginMapper.toCustomPluginDto(plugin));
        }
        return plugins;
    }

    @Override
    public PipelineDescriptor findCustomPluginById(User user, String pluginId) {
        Optional<CustomPlugin> plugin = findSpecificCustomPluginById(user, pluginId);
        if (plugin.isPresent()) {
            return plugin.map(CustomPlugin::getPluginDescriptor).get().setPipelineId(pluginId);
        }
        throw CpPluginException.throwException(CUSTOM_PLUGIN, NOT_FOUND, pluginId);
    }

    @Override
    public CustomPluginDto addCustomPlugin(
            User user,
            String pluginName,
            PipelineDescriptor descriptor,
            List<CustomNodeIO> inputs,
            List<CustomNodeIO> outputs,
            String subtitle) {
        if (findSpecificCustomPluginByName(user, pluginName).isEmpty()) {
            Date now = new Date();
            CustomPlugin customPlugin =
                    new CustomPlugin()
                            .setPluginDescriptor(descriptor)
                            .setCreationDate(now)
                            .setFileName(pluginName)
                            .setInputs(inputs)
                            .setOutputs(outputs)
                            .setLastModificationDate(now)
                            .setUser(user)
                            .setNodeClass("CUSTOM")
                            .setType("C")
                            .setSubtitle(subtitle);
            return CustomPluginMapper.toCustomPluginDto(customPluginRepository.save(customPlugin));
        }
        throw CpPluginException.throwException(CUSTOM_PLUGIN, DUPLICATE_ENTITY, pluginName);
    }

    @Override
    public CustomPluginDto updateCustomPlugin(
            User user,
            String pluginId,
            String pluginName,
            PipelineDescriptor descriptor,
            List<CustomNodeIO> inputs,
            List<CustomNodeIO> outputs,
            String subtitle) {
        Optional<CustomPlugin> customPlugin = findSpecificCustomPluginById(user, pluginId);
        if (customPlugin.isPresent()) {
            Date now = new Date();
            customPlugin
                    .get()
                    .setLastModificationDate(now)
                    .setInputs(inputs)
                    .setOutputs(outputs)
                    .setFileName(pluginName)
                    .setPluginDescriptor(descriptor)
                    .setSubtitle(subtitle);
            return CustomPluginMapper.toCustomPluginDto(customPluginRepository.save(customPlugin.get()));
        }
        throw CpPluginException.throwException(CUSTOM_PLUGIN, NOT_FOUND, pluginId);
    }

    @Override
    public void removeCustomPlugin(User user, String customPluginId) {
        Optional<CustomPlugin> customPlugin = findSpecificCustomPluginById(user, customPluginId);
        if (customPlugin.isPresent()) {
            customPluginRepository.delete(customPlugin.get());
        } else {
            throw CpPluginException.throwException(CUSTOM_PLUGIN, NOT_FOUND, customPluginId);
        }
    }

    private Optional<CustomPlugin> findSpecificCustomPluginById(User user, String customPluginId) {
        return customPluginRepository.findCustomPluginsByUser(user).stream()
                .filter(f -> f.getId().equalsIgnoreCase(customPluginId))
                .findFirst();
    }

    private Optional<CustomPlugin> findSpecificCustomPluginByName(
            User user, String customPluginName) {
        return customPluginRepository.findCustomPluginsByUser(user).stream()
                .filter(f -> f.getFileName().equalsIgnoreCase(customPluginName))
                .findFirst();
    }
}
