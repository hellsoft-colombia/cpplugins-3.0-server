package co.edu.javeriana.hellsoft.cpplugins.service.core;

import co.edu.javeriana.hellsoft.cpplugins.dto.model.ExecutionHistoryDto;
import co.edu.javeriana.hellsoft.cpplugins.dto.model.ImageDto;
import co.edu.javeriana.hellsoft.cpplugins.model.User;

import java.util.List;

public interface ExecutionHistoryService {
    void addHistoryReference(
            String executionState,
            String pipelineIdReference,
            String pipelineNameReference,
            String pipelineDescriptionReference,
            String executionResult,
            User user,
            List<ImageDto> imageResult);

    List<ExecutionHistoryDto> getUserExecutionHistory(User user, int elementsPerPage, int page);

    Long getUserExecutionHistoryCount(User user);

    List<ExecutionHistoryDto> getUserExecutionHistoryByState(
            User user, String state, int elementsPerPage, int page);

    Long getUserExecutionHistoryByStateCount(User user, String state);

    ExecutionHistoryDto getExecutionHistoryById(User user, String id);
}
