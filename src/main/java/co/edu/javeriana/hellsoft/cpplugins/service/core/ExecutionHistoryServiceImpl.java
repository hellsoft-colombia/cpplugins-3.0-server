package co.edu.javeriana.hellsoft.cpplugins.service.core;

import co.edu.javeriana.hellsoft.cpplugins.dto.mapper.ExecutionHistoryMapper;
import co.edu.javeriana.hellsoft.cpplugins.dto.model.ExecutionHistoryDto;
import co.edu.javeriana.hellsoft.cpplugins.dto.model.ImageDto;
import co.edu.javeriana.hellsoft.cpplugins.exception.CpPluginException;
import co.edu.javeriana.hellsoft.cpplugins.model.ExecutionHistory;
import co.edu.javeriana.hellsoft.cpplugins.model.User;
import co.edu.javeriana.hellsoft.cpplugins.repository.ExecutionHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static co.edu.javeriana.hellsoft.cpplugins.dto.mapper.ExecutionHistoryMapper.toExecutionHistoryDto;
import static co.edu.javeriana.hellsoft.cpplugins.exception.EntityType.HISTORY_REGISTER;
import static co.edu.javeriana.hellsoft.cpplugins.exception.ExceptionType.NOT_FOUND;

@Component
public class ExecutionHistoryServiceImpl implements ExecutionHistoryService {

    @Autowired
    private ExecutionHistoryRepository executionHistoryRepository;

    @Override
    public void addHistoryReference(
            String executionState,
            String pipelineIdReference,
            String pipelineNameReference,
            String pipelineDescriptionReference,
            String executionResult,
            User user,
            List<ImageDto> imageResult) {
        ExecutionHistory historyInput =
                new ExecutionHistory()
                        .setExecutionResult(executionResult)
                        .setExecutionTime(new Date())
                        .setPipelineIdReference(pipelineIdReference)
                        .setPipelineNameReference(pipelineNameReference)
                        .setState(executionState)
                        .setUser(user)
                        .setPipelineDescriptionReference(pipelineDescriptionReference)
                        .setImageResults(imageResult);
        executionHistoryRepository.save(historyInput);
    }

    @Override
    public List<ExecutionHistoryDto> getUserExecutionHistory(
            User user, int elementsPerPage, int page) {
        Pageable pageable =
                PageRequest.of(page, elementsPerPage, Sort.by(Sort.Direction.DESC, "executionTime"));
        Page<ExecutionHistory> histories = executionHistoryRepository.findByUser(user, pageable);
        return histories
                .get()
                .map(ExecutionHistoryMapper::toExecutionHistoryDto)
                .collect(Collectors.toList());
    }

    @Override
    public Long getUserExecutionHistoryCount(User user) {
        return executionHistoryRepository.countByUser(user);
    }

    @Override
    public List<ExecutionHistoryDto> getUserExecutionHistoryByState(
            User user, String state, int elementsPerPage, int page) {
        Pageable pageable =
                PageRequest.of(page, elementsPerPage, Sort.by(Sort.Direction.DESC, "executionTime"));
        Page<ExecutionHistory> histories =
                executionHistoryRepository.findByUserAndState(user, state, pageable);
        return histories
                .get()
                .map(ExecutionHistoryMapper::toExecutionHistoryDto)
                .collect(Collectors.toList());
    }

    @Override
    public Long getUserExecutionHistoryByStateCount(User user, String state) {
        return executionHistoryRepository.countByUserAndState(user, state);
    }

    @Override
    public ExecutionHistoryDto getExecutionHistoryById(User user, String id) {
        Optional<ExecutionHistory> executionHistory =
                executionHistoryRepository.findByUserAndId(user, id);
        if (executionHistory.isPresent()) return toExecutionHistoryDto(executionHistory.get());
        throw CpPluginException.throwException(HISTORY_REGISTER, NOT_FOUND, id);
    }
}
