package co.edu.javeriana.hellsoft.cpplugins.service.core;

import co.edu.javeriana.hellsoft.cpplugins.dto.model.ImageDto;
import co.edu.javeriana.hellsoft.cpplugins.model.User;
import org.springframework.core.io.Resource;

import java.util.List;

public interface ImageService {
    List<ImageDto> findUserImages(User user);

    ImageDto addImage(User user, String fileName, String format, float size, byte[] fileContent);

    ImageDto addImageResult(User user, String fileName, String format, float size, String newPath);

    void removeImage(User user, String imageId);

    Resource getImage(User user, String imageId);
}
