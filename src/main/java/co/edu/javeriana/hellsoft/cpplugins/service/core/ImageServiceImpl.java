package co.edu.javeriana.hellsoft.cpplugins.service.core;

import co.edu.javeriana.hellsoft.cpplugins.dto.mapper.ImageMapper;
import co.edu.javeriana.hellsoft.cpplugins.dto.model.ImageDto;
import co.edu.javeriana.hellsoft.cpplugins.exception.CpPluginException;
import co.edu.javeriana.hellsoft.cpplugins.model.Image;
import co.edu.javeriana.hellsoft.cpplugins.model.User;
import co.edu.javeriana.hellsoft.cpplugins.repository.ImageRepository;
import co.edu.javeriana.hellsoft.cpplugins.service.file.NativeFileHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static co.edu.javeriana.hellsoft.cpplugins.exception.EntityType.IMAGE;
import static co.edu.javeriana.hellsoft.cpplugins.exception.ExceptionType.*;

@Component
public class ImageServiceImpl implements ImageService {

    @Value("${file.server.path}")
    private String path;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private NativeFileHandle nativeFileHandle;

    @Override
    public List<ImageDto> findUserImages(User user) {
        List<ImageDto> images = new ArrayList<>();
        Set<Image> imagesSet = imageRepository.findImagesByUser(user);
        for (Image image : imagesSet) images.add(ImageMapper.toImageDto(image));
        return images;
    }

    @Override
    public ImageDto addImage(
            User user, String fileName, String format, float size, byte[] fileContent) {
        Optional<Image> image = Optional.ofNullable(imageRepository.findByFileName(fileName));

        if (image.isEmpty()) {
            Image newImage;
            String folderPath = path + "/" + user.getId() + "/resources/";
            Path imagePath = Paths.get(folderPath + fileName);
            nativeFileHandle.createFolder(folderPath);
            try {
                Files.write(imagePath, fileContent);
                Date now = new Date();
                newImage =
                        new Image()
                                .setFileName(fileName)
                                .setCreationDate(now)
                                .setFormat(format)
                                .setImageSize(size)
                                .setPath(imagePath.normalize().toString())
                                .setUser(user);
                return ImageMapper.toImageDto(imageRepository.save(newImage));
            } catch (IOException e) {
                throw CpPluginException.throwException(IMAGE, INTERNAL_SERVER_ERROR, fileName);
            }
        }
        throw CpPluginException.throwException(IMAGE, DUPLICATE_ENTITY, fileName);
    }

    @Override
    public ImageDto addImageResult(User user, String fileName, String format, float size, String newPath) {

        Image newImage;
        Path imagePath = Paths.get(newPath + fileName);

        Date now = new Date();
        newImage =
                new Image()
                        .setFileName(fileName)
                        .setCreationDate(now)
                        .setFormat(format)
                        .setImageSize(size)
                        .setPath(imagePath.normalize().toString())
                        .setUser(user);
        return ImageMapper.toImageDto(imageRepository.save(newImage));
    }

    @Override
    public void removeImage(User user, String imageId) {
        Optional<Image> image = imageRepository.findById(imageId);
        if (image.isPresent()) {
            String folderPath = path + "/" + user.getId() + "/";
            String imagePath = folderPath + image.get().getFileName();
            nativeFileHandle.removeFile(imagePath);
            imageRepository.delete(image.get());
        } else throw CpPluginException.throwException(IMAGE, NOT_FOUND, imageId);
    }

    @Override
    public Resource getImage(User user, String imageId) {
        Optional<Image> imageInfo = imageRepository.findById(imageId);

        if (imageInfo.isPresent()) {
            String folderPath = path + "/" + user.getId() + "/";
            Path imagePath = Paths.get(folderPath + imageInfo.get().getFileName() + imageInfo.get().getFormat());

            Resource image;

            try {
                image = new UrlResource(imagePath.toUri());
            } catch (MalformedURLException e) {
                throw CpPluginException.throwException(IMAGE, INTERNAL_SERVER_ERROR, imageId);
            }

            if (!image.isReadable() || !image.exists())
                throw CpPluginException.throwException(IMAGE, NOT_FOUND, imageId);
            return image;
        }

        throw CpPluginException.throwException(IMAGE, NOT_FOUND, imageId);
    }
}
