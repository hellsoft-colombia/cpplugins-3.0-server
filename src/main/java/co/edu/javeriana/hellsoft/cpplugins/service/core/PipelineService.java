package co.edu.javeriana.hellsoft.cpplugins.service.core;

import co.edu.javeriana.hellsoft.cpplugins.dto.model.PipelineDto;
import co.edu.javeriana.hellsoft.cpplugins.messages.FinishedPipeline;
import co.edu.javeriana.hellsoft.cpplugins.model.User;
import co.edu.javeriana.hellsoft.cpplugins.model.pipeline.PipelineDescriptor;

import java.util.List;

public interface PipelineService {
    PipelineDto addPipeline(
            User user,
            String pipelineName,
            String pipelineDescription,
            PipelineDescriptor pipelineContent);

    PipelineDto updatePipeline(
            User user, String pipelineName, String pipelineId, PipelineDescriptor pipelineContent);

    void removePipeline(User user, String pipelineId);

    PipelineDescriptor findPipelineById(User user, String pipelineId);

    PipelineDescriptor replaceCustomFilterToDescriptor(PipelineDescriptor descriptor);

    void runPipeline(User user, PipelineDescriptor pipeline);

    void handleFinishedPipeline(FinishedPipeline finishedPipeline);

    void changePipelineExecutionState(User user, String state, String pipelineId);

    List<PipelineDto> findUserPipelines(User user);

    List<PipelineDto> findUserPipelinesByState(User user, String state);
}
