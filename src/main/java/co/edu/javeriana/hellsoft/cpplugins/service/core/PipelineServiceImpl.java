package co.edu.javeriana.hellsoft.cpplugins.service.core;

import co.edu.javeriana.hellsoft.cpplugins.dto.mapper.PipelineMapper;
import co.edu.javeriana.hellsoft.cpplugins.dto.model.ImageDto;
import co.edu.javeriana.hellsoft.cpplugins.dto.model.PipelineDto;
import co.edu.javeriana.hellsoft.cpplugins.exception.CpPluginException;
import co.edu.javeriana.hellsoft.cpplugins.messages.FinishedPipeline;
import co.edu.javeriana.hellsoft.cpplugins.model.Pipeline;
import co.edu.javeriana.hellsoft.cpplugins.model.User;
import co.edu.javeriana.hellsoft.cpplugins.model.customplugin.CustomPlugin;
import co.edu.javeriana.hellsoft.cpplugins.model.pipeline.PipelineDescriptor;
import co.edu.javeriana.hellsoft.cpplugins.model.pipeline.node.Node;
import co.edu.javeriana.hellsoft.cpplugins.model.util.Email;
import co.edu.javeriana.hellsoft.cpplugins.repository.CustomPluginRepository;
import co.edu.javeriana.hellsoft.cpplugins.repository.PipelineRepository;
import co.edu.javeriana.hellsoft.cpplugins.service.ExecutionState;
import co.edu.javeriana.hellsoft.cpplugins.service.file.NativeFileHandle;
import co.edu.javeriana.hellsoft.cpplugins.service.util.EmailService;
import co.edu.javeriana.hellsoft.cpplugins.wrappers.CpPluginsInterface;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static co.edu.javeriana.hellsoft.cpplugins.exception.EntityType.PIPELINE;
import static co.edu.javeriana.hellsoft.cpplugins.exception.ExceptionType.*;
import static co.edu.javeriana.hellsoft.cpplugins.service.ExecutionState.NOT_EXECUTING;

@Component
public class PipelineServiceImpl implements PipelineService {

    @Autowired
    private ImageService imageService;
    @Autowired
    private PipelineRepository pipelineRepository;
    @Autowired
    private CustomPluginRepository customPluginRepository;
    @Autowired
    private ExecutionHistoryService executionHistoryService;
    @Autowired
    private CpPluginsInterface cpPluginsInterface;
    @Autowired
    private UserService userService;
    @Autowired
    private ObjectMapper mapper;
    @Value("${broker.url}")
    private String brokerUrl;
    @Value("${file.server.path}")
    private String path;
    @Autowired
    private NativeFileHandle nativeFileHandle;
    @Autowired
    private EmailService emailService;

    @Override
    public PipelineDto addPipeline(
            User user,
            String pipelineName,
            String pipelineDescription,
            PipelineDescriptor pipelineContent) {
        if (findSpecificPipelineByName(user, pipelineName).isEmpty()) {
            Date now = new Date();
            Pipeline newPipeline =
                    new Pipeline()
                            .setDescription(pipelineDescription)
                            .setCreationDate(now)
                            .setFileName(pipelineName)
                            .setLastModificationDate(now)
                            .setState(NOT_EXECUTING.state())
                            .setPipelineDescriptor(pipelineContent)
                            .setUser(user);
            return PipelineMapper.toPipelineDto(pipelineRepository.save(newPipeline));
        }
        throw CpPluginException.throwException(PIPELINE, DUPLICATE_ENTITY, pipelineName);
    }

    @Override
    public PipelineDto updatePipeline(
            User user, String pipelineName, String pipelineId, PipelineDescriptor pipelineContent) {
        Optional<Pipeline> specificPipeline = findSpecificPipelineById(user, pipelineId);
        if (specificPipeline.isPresent()) {
            Date now = new Date();
            specificPipeline
                    .get()
                    .setLastModificationDate(now)
                    .setFileName(pipelineName)
                    .setPipelineDescriptor(pipelineContent);
            return PipelineMapper.toPipelineDto(pipelineRepository.save(specificPipeline.get()));
        }
        throw CpPluginException.throwException(PIPELINE, NOT_FOUND, pipelineId);
    }

    @Override
    public void removePipeline(User user, String pipelineId) {
        Optional<Pipeline> specificPipeline = findSpecificPipelineById(user, pipelineId);
        if (specificPipeline.isPresent()) {
            pipelineRepository.delete(specificPipeline.get());
        } else {
            throw CpPluginException.throwException(PIPELINE, NOT_FOUND, pipelineId);
        }
    }

    @Override
    public PipelineDescriptor findPipelineById(User user, String pipelineId) {
        Optional<Pipeline> specificPipeline = findSpecificPipelineById(user, pipelineId);
        if (specificPipeline.isPresent()) {
            return specificPipeline.map(Pipeline::getPipelineDescriptor).get().setPipelineId(pipelineId);
        }
        throw CpPluginException.throwException(PIPELINE, NOT_FOUND, pipelineId);
    }

    @Override
    public PipelineDescriptor replaceCustomFilterToDescriptor(PipelineDescriptor descriptor) {
        for (Node node : descriptor.getNodes()) {
            if (node.getType().equalsIgnoreCase("Custom") | node.getType().equalsIgnoreCase("C") | node.getType().equalsIgnoreCase("CustomNode")) {
                Optional<CustomPlugin> customPlugin = customPluginRepository.findById(node.getPluginId());
                customPlugin.ifPresent(plugin -> node.setDescriptorDesign(plugin.getPluginDescriptor()));
            }
        }
        return descriptor;
    }

    @Override
    public void runPipeline(User user, PipelineDescriptor pipeline) {
        String folderPath = path + "/" + user.getId() + "/resources/";
        try {
            cpPluginsInterface.executePipeline(mapper.writeValueAsString(pipeline), folderPath, brokerUrl);
        } catch (JsonProcessingException e) {
            throw CpPluginException.throwException(PIPELINE, BAD_FORMATTED_DATA, pipeline.getPipelineId());
        }
        changePipelineExecutionState(user, ExecutionState.EXECUTING.state(), pipeline.getPipelineId());
    }

    @Override
    public void handleFinishedPipeline(FinishedPipeline finishedPipeline) {
        Optional<Pipeline> pipeline = this.findSpecificPipelineById(finishedPipeline.getPipelineId());
        pipeline.ifPresent(pipeline1 -> {
            User user = pipeline1.getUser();
            List<ImageDto> imageResult = new ArrayList<>();
            String resultPath = path + "/" + finishedPipeline.getPipelineId() + "/" + "pipelines" + finishedPipeline.getFolderResult();
            List<String> files = nativeFileHandle.filesInFolder(resultPath);
            files.forEach(file -> {
                imageResult.add(imageService.addImageResult(user, file, file.substring(file.lastIndexOf(".")), nativeFileHandle.getFileSize(resultPath + "/" + file), resultPath + "/" + file));
            });
            changePipelineExecutionState(user, ExecutionState.FINISHED.state(), pipeline1.getId(), finishedPipeline.getResult(), imageResult);
            emailService.sendHtml(new Email()
                            .setTo(user.getEmail())
                            .setSubject(pipeline1.getFileName() + " Finished execution ! ")
                            .setBody("Hi " + user.getFirstName() + ", your pipeline " + pipeline1.getFileName() +
                                    " have finished it execution with the result: " + finishedPipeline.getResult() +
                                    " in about " + finishedPipeline.getElapsedTime() + "ms."),
                    "Finished " + pipeline1.getFileName() + " Execution");
        });
    }

    @Override
    public void changePipelineExecutionState(User user, String state, String pipelineId) {
        Optional<Pipeline> specificPipeline = findSpecificPipelineById(user, pipelineId);
        specificPipeline.ifPresent(pipeline -> pipelineRepository.save(pipeline.setState(state)));
        specificPipeline.ifPresent(pipeline ->
                executionHistoryService.addHistoryReference(
                        state,
                        pipelineId,
                        pipeline.getFileName(),
                        pipeline.getDescription(),
                        "",
                        user,
                        new ArrayList<>()));
    }

    private void changePipelineExecutionState(User user, String state, String pipelineId, String executionResult, List<ImageDto> results) {
        Optional<Pipeline> specificPipeline = findSpecificPipelineById(user, pipelineId);
        specificPipeline.ifPresent(pipeline -> pipelineRepository.save(pipeline.setState(state)));
        specificPipeline.ifPresent(pipeline ->
                executionHistoryService.addHistoryReference(
                        state,
                        pipelineId,
                        pipeline.getFileName(),
                        pipeline.getDescription(),
                        executionResult,
                        user,
                        results));
    }

    @Override
    public List<PipelineDto> findUserPipelines(User user) {
        return pipelineRepository.findPipelinesByUser(user).stream()
                .map(PipelineMapper::toPipelineDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<PipelineDto> findUserPipelinesByState(User user, String state) {
        return pipelineRepository.findPipelinesByUser(user).stream()
                .filter(f -> f.getState().equalsIgnoreCase(state))
                .map(PipelineMapper::toPipelineDto)
                .collect(Collectors.toList());
    }

    private Optional<Pipeline> findSpecificPipelineById(User user, String pipelineId) {
        return pipelineRepository.findPipelinesByUser(user).stream()
                .filter(f -> f.getId().equalsIgnoreCase(pipelineId))
                .findFirst();
    }

    private Optional<Pipeline> findSpecificPipelineById(String pipelineId) {
        return pipelineRepository.findById(pipelineId).stream()
                .filter(f -> f.getId().equalsIgnoreCase(pipelineId))
                .findFirst();
    }

    private Optional<Pipeline> findSpecificPipelineByName(User user, String fileName) {
        return pipelineRepository.findPipelinesByUser(user).stream()
                .filter(f -> f.getFileName().equalsIgnoreCase(fileName))
                .findFirst();
    }
}
