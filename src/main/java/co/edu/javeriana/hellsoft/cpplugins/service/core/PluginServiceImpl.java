package co.edu.javeriana.hellsoft.cpplugins.service.core;

import co.edu.javeriana.hellsoft.cpplugins.wrappers.CpPluginsInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PluginServiceImpl implements PluginService {

    @Autowired
    private CpPluginsInterface cpPluginsInterface;

    @Override
    public String getSystemPlugins() {
        return cpPluginsInterface.GetPlugins();
    }
}
