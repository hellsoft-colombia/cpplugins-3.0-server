package co.edu.javeriana.hellsoft.cpplugins.service.core;

import co.edu.javeriana.hellsoft.cpplugins.dto.model.UserDto;
import co.edu.javeriana.hellsoft.cpplugins.model.User;

import java.util.Optional;

public interface UserService {
    UserDto signUp(UserDto userDto);

    UserDto findUserByUserName(String userName);

    User findNativeUserByUserName(String userName);

    Optional<User> findUserById(String userId);
}
