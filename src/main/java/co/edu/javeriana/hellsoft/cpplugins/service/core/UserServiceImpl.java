package co.edu.javeriana.hellsoft.cpplugins.service.core;

import co.edu.javeriana.hellsoft.cpplugins.dto.mapper.UserMapper;
import co.edu.javeriana.hellsoft.cpplugins.dto.model.UserDto;
import co.edu.javeriana.hellsoft.cpplugins.exception.CpPluginException;
import co.edu.javeriana.hellsoft.cpplugins.model.User;
import co.edu.javeriana.hellsoft.cpplugins.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static co.edu.javeriana.hellsoft.cpplugins.exception.EntityType.USER;
import static co.edu.javeriana.hellsoft.cpplugins.exception.ExceptionType.DUPLICATE_ENTITY;
import static co.edu.javeriana.hellsoft.cpplugins.exception.ExceptionType.NOT_FOUND;

@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDto signUp(UserDto userDto) {
        Optional<User> user = findUser(userDto.getUserName());
        if (user.isEmpty()) {
            User newUser =
                    new User()
                            .setEmail(userDto.getEmail())
                            .setFirstName(userDto.getFirstName())
                            .setLastName(userDto.getLastName())
                            .setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()))
                            .setUserName(userDto.getUserName());
            return UserMapper.toUserDto(userRepository.save(newUser));
        }
        throw CpPluginException.throwException(USER, DUPLICATE_ENTITY, userDto.getUserName());
    }

    @Override
    public UserDto findUserByUserName(String userName) {
        Optional<User> user = findUser(userName);
        if (user.isEmpty()) {
            throw CpPluginException.throwException(USER, NOT_FOUND, userName);
        }
        return modelMapper.map(user.get(), UserDto.class);
    }

    @Override
    public Optional<User> findUserById(String userId) {
        return userRepository.findById(userId);
    }

    @Override
    public User findNativeUserByUserName(String userName) {
        Optional<User> user = findUser(userName);
        if (user.isPresent()) {
            return user.get();
        }
        throw CpPluginException.throwException(USER, NOT_FOUND, userName);
    }

    private Optional<User> findUser(String userName) {
        return Optional.ofNullable(userRepository.findUserByUserNameEquals(userName));
    }

}
