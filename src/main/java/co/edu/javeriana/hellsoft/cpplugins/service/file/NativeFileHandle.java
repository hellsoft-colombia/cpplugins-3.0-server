package co.edu.javeriana.hellsoft.cpplugins.service.file;

import java.util.List;

public interface NativeFileHandle {
    void removeFile(String path);

    void createFolder(String path);

    List<String> filesInFolder(String path);

    float getFileSize(String path);
}
