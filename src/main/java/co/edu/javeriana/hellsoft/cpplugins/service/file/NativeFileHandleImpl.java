package co.edu.javeriana.hellsoft.cpplugins.service.file;

import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Component
public class NativeFileHandleImpl implements NativeFileHandle {
    @Override
    public void removeFile(String path) {
        File file = new File(path);
        file.delete();
    }

    @Override
    public void createFolder(String path) {
        File directory = new File(path);
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }

    @Override
    public List<String> filesInFolder(String path) {
        List<String> results = new ArrayList<String>();
        File[] files = new File(path).listFiles();
        for (File file : files) {
            if (file.isFile()) {
                results.add(file.getName());
            }
        }
        return results;
    }

    @Override
    public float getFileSize(String path) {
        File file = new File(path);
        return (!file.exists() || !file.isFile()) ? 0 : file.length() / (1024 * 1024);
    }
}
