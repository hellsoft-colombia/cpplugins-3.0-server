package co.edu.javeriana.hellsoft.cpplugins.service.util;

import co.edu.javeriana.hellsoft.cpplugins.model.util.Email;
import co.edu.javeriana.hellsoft.cpplugins.model.util.EmailResponse;

public interface EmailService {
    EmailResponse sendHtml(Email email, String title);

    EmailResponse sendText(Email email);
}
