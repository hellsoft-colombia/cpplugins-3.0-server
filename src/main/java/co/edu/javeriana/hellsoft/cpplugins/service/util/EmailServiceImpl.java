package co.edu.javeriana.hellsoft.cpplugins.service.util;

import co.edu.javeriana.hellsoft.cpplugins.model.util.Email;
import co.edu.javeriana.hellsoft.cpplugins.model.util.EmailResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.MimeMessage;

@Component
public class EmailServiceImpl implements EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private TemplateEngine templateEngine;

    @Override
    public EmailResponse sendText(Email email) {
        return sendMail(email, false);
    }

    @Override
    public EmailResponse sendHtml(Email email, String title) {
        Context context = new Context();
        context.setVariable("title", title);
        context.setVariable("description", email.getBody());
        email.setBody(templateEngine.process("email/template-1", context));
        return sendMail(email, true);
    }

    private EmailResponse sendMail(Email email, boolean isHtml) {
        try {
            MimeMessage mail = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(email.getTo());
            helper.setSubject(email.getSubject());
            helper.setText(email.getBody(), isHtml);
            javaMailSender.send(mail);
            return EmailResponse.success();
        } catch (Exception e) {
            return EmailResponse.error(e.toString());
        }
    }
}
