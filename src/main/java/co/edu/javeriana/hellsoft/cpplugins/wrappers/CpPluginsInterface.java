package co.edu.javeriana.hellsoft.cpplugins.wrappers;

public class CpPluginsInterface {

    public native String GetPlugins();

    public native void executePipeline(String pipeline, String resourcePath, String jmqAddress);

}
