#include "co_edu_javeriana_hellsoft_cpplugins_wrappers_CpPluginsInterface.h"
#include <fstream>
#include <iostream>
#include <cpPlugins/Manager.h>

/* Definition */
typedef cpPlugins::Manager       TManager;
typedef TManager::TPlugins       TPlugins;
typedef TManager::TCategories    TCategories;

/* Util methods TODO: Pass to another class */
TManager initcpPluginsManager(std::string path){
    TManager mgr;
    mgr.Configure(path);
    return mgr;
}

/*
 * Method:    jstring2string
 * Description: Parse Java String to c++ once
 * Take from Slerte
 * Link: https://stackoverflow.com/questions/41820039/jstringjni-to-stdstringc-with-utf8-characters
 */
std::string jstringToString(JNIEnv *env, jstring jStr) {
    if (!jStr)
        return "";

    const jclass stringClass = (*env).GetObjectClass(jStr);
    const jmethodID getBytes = (*env).GetMethodID(stringClass, "getBytes", "(Ljava/lang/String;)[B");
    const jbyteArray stringJbytes = (jbyteArray) (*env).CallObjectMethod(jStr, getBytes, (*env).NewStringUTF("UTF-8"));

    size_t length = (size_t) (*env).GetArrayLength(stringJbytes);
    jbyte* pBytes = (*env).GetByteArrayElements(stringJbytes, NULL);

    std::string result = std::string((char *)pBytes, length);
    (*env).ReleaseByteArrayElements(stringJbytes, pBytes, JNI_ABORT);

    (*env).DeleteLocalRef(stringJbytes);
    (*env).DeleteLocalRef(stringClass);
    return result;
}

/*
 * Method:    pluginsToJObject
 * Description: Parse c++ map in plugins to Java HashMap <String, String>
 */
jobject pluginsToJObject( JNIEnv* env, TPlugins pluginSet)
{
	jclass clazz = (*env).FindClass("java/util/HashMap");
	jmethodID init = (*env).GetMethodID(clazz, "<init>", "()V");
	jobject pluginSetMap = (*env).NewObject(clazz, init);

	jmethodID putMethod = (*env).GetMethodID(
		clazz, 
		"put", 
		"(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
	);

	for( auto it = pluginSet.begin(); it != pluginSet.end(); ++it )
	{
		jstring key = (*env).NewStringUTF( (*it).first.c_str() );
		jstring value = (*env).NewStringUTF( (*it).second.c_str() );

		(*env).CallObjectMethod(
			pluginSetMap, 
			putMethod,
			key,
			value
		);
	}

	return pluginSetMap;
}


/*
 * Method:    categoriesToJObject
 * Description: Parse c++ map in categories to Java HashMap HashMap<String, HashMap<String,String>>
 */
jobject categoriesToJObject( JNIEnv* env, TCategories categoriesSet)
{
	jclass clazz = (*env).FindClass("java/util/HashMap");
	jmethodID init = (*env).GetMethodID(clazz, "<init>", "()V");
	jobject categorySetMap = (*env).NewObject(clazz, init);

	jmethodID putMethod = (*env).GetMethodID(
		clazz, 
		"put", 
		"(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
	);

	for( auto it = categoriesSet.begin(); it != categoriesSet.end(); ++it )
	{
		jstring key = (*env).NewStringUTF( (*it).first.c_str() );
		jobject value = pluginsToJObject(env, (*it).second );

		(*env).CallObjectMethod(
			categorySetMap, 
			putMethod,
			key,
			value
		);
	}

	return categorySetMap;
}

/*
 * Class:     co_edu_javeriana_hellsoft_cpplugins_wrappers_CpPluginsInterface
 * Method:    GetCategories
 * Signature: ()Ljava/util/HashMap;
 */
JNIEXPORT jobject JNICALL Java_co_edu_javeriana_hellsoft_cpplugins_wrappers_CpPluginsInterface_GetCategories
  (JNIEnv *env, jobject thisObject) {
    TManager mgr = initcpPluginsManager("./");
    TCategories cpPluginsCats = mgr.GetCategories();
    return categoriesToJObject(env,cpPluginsCats);
}

/*
 * Class:     co_edu_javeriana_hellsoft_cpplugins_wrappers_CpPluginsInterface
 * Method:    GetPlugins
 * Signature: (Ljava/lang/String;)Ljava/util/HashMap;
 */
JNIEXPORT jstring JNICALL Java_co_edu_javeriana_hellsoft_cpplugins_wrappers_CpPluginsInterface_GetPlugins
  (JNIEnv *env, jobject thisObject) {
    /*TManager mgr = initcpPluginsManager("./");
    std::string selectedCategorie = jstringToString(env, categorie);
    TPlugins cpPluginsPlugs = mgr.GetPlugins(selectedCategorie);
    return pluginsToJObject(env,cpPluginsPlugs);*/

    std::string test = R"({"functionList":[{"title":"Widget","subtitle":"Esto es la descripción de un widget","isDirectory":true,"children":[{"title":"Paquete1","subtitle":"Esto es la descripción de un paquete1","isDirectory":true,"children":[{"title":"Widget1","type":"W","nodeClass":"TestClass","subtitle":"Esto es la descripción de un widget1","inputs":[{"title":"input1","id":0},{"title":"input2","id":0}],"outputs":[{"title":"output1","id":0}],"parameters":["parameter1","patameter2"]},{"title":"Widget2","type":"W","nodeClass":"TestClass","subtitle":"Esto es la descripción de un widget2","inputs":[{"title":"input1","id":0},{"title":"input2","id":0}],"outputs":[{"title":"output1","id":0}],"parameters":["parameter1","patameter2"]},{"title":"Widget3","type":"W","nodeClass":"TestClass","subtitle":"Esto es la descripción de un widget3","inputs":[{"title":"input1","id":0},{"title":"input2","id":0}],"outputs":[{"title":"output1","id":0}],"parameters":["parameter1","patameter2"]}]}]},{"title":"Functor","subtitle":"Esto es la descripción de un functor","isDirectory":true,"children":[{"title":"Paquete1","subtitle":"Esto es la descripción de un paquete1","isDirectory":true,"children":[{"title":"Functor1","type":"Fn","nodeClass":"TestClass","subtitle":"Esto es la descripción de un functor1","inputs":[{"title":"input1","id":0},{"title":"input2","id":0}],"outputs":[{"title":"output1","id":0}],"parameters":["parameter1","patameter2"]},{"title":"Functor2","type":"Fn","nodeClass":"TestClass","subtitle":"Esto es la descripción de un functor2","inputs":[{"title":"input1","id":0},{"title":"input2","id":0}],"outputs":[{"title":"output1","id":0}],"parameters":["parameter1","patameter2"]},{"title":"Functor3","type":"Fn","nodeClass":"TestClass","subtitle":"Esto es la descripción de un functor3","inputs":[{"title":"input1","id":0},{"title":"input2","id":0}],"outputs":[{"title":"output1","id":0}],"parameters":["parameter1","patameter2"]}]}]},{"title":"Filter","subtitle":"Esto es la descripción de un filter","isDirectory":true,"children":[{"title":"Paquete3","subtitle":"Esto es la descripción de un paquete1","isDirectory":true,"children":[{"title":"Filter1","type":"Fl","nodeClass":"TestClass","subtitle":"Esto es la descripción de un filter1","inputs":[{"title":"input1","id":0},{"title":"input2","id":0}],"outputs":[{"title":"output1","id":0}],"parameters":["parameter1","patameter2"]},{"title":"Filter2","type":"Fl","nodeClass":"TestClass","subtitle":"Esto es la descripción de un filter2","inputs":[{"title":"input1","id":0},{"title":"input2","id":0}],"outputs":[{"title":"output1","id":0}],"parameters":["parameter1","parameter2","parameter3","parameter4","parameter5","parameter6"]},{"title":"Filter3","type":"Fl","nodeClass":"TestClass","subtitle":"Esto es la descripción de un filter3","inputs":[{"title":"input1","id":0},{"title":"input2","id":0}],"outputs":[{"title":"output1","id":0}],"parameters":["parameter1","patameter2"]}]},{"title":"Readers","subtitle":"Paquete que tiene los filtros que leen imagenes","isDirectory":true,"children":[{"title":"reader","type":"Fl","nodeClass":"Reader","subtitle":"Un reader cualquiera","inputs":[],"outputs":[],"parameters":["imageLocation"]},{"title":"writer","type":"Fl","nodeClass":"Writer","subtitle":"Un writer cualquiera","inputs":[],"outputs":[],"parameters":["param1","param2"]}]}]}]})";
    return env->NewStringUTF(test.c_str());
}

/*
 * Class:     co_edu_javeriana_hellsoft_cpplugins_wrappers_CpPluginsInterface
 * Method:    executePipeline
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_co_edu_javeriana_hellsoft_cpplugins_wrappers_CpPluginsInterface_executePipeline
  (JNIEnv *env, jobject thisObject, jstring pipeline, jstring resourcePath, jstring jmqAddress){
    //Leo stuff :D
  }